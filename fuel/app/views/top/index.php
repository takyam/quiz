<p class="pull-right">
	<a href="/top/create" class="btn btn-primary"><i class="icon-plus icon-white"></i> ついか</a>
</p>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>問題</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($questions as $question): ?>
		<tr>
			<td><?php echo $question->id; ?></td>
			<td>
				<a href="/top/show/<?php echo $question->id; ?>">
					<?php echo $question->title; ?>
				</a>
			</td>
			<td>
				<a href="/top/show/<?php echo $question->id; ?>" class="btn btn-mini">みる</a>
				<a href="/top/create/<?php echo $question->id; ?>" class="btn btn-mini">へんしゅう</a>
				<a href="/top/delete/<?php echo $question->id; ?>" class="btn btn-mini btn-danger"
					onclick="return confirm('ほんとにけすよ？');">けす</a>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>