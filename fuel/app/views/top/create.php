<?php echo Form::open(array('method' => 'post')); ?>
	<?php echo Form::input('title', $params['title'], array('placeholder' => 'もんだい', 'class' => 'span12')); ?>
	<?php if(isset($errors['title'])): ?><?php echo View::forge('elements/errors', array('errors' => $errors['title'])); ?><?php endif; ?>
	<?php echo Form::textarea('description', $params['description'], array('placeholder' => 'もんだいのせつめい', 'class' => 'span12')); ?>
	<?php if(isset($errors['description'])): ?><?php echo View::forge('elements/errors', array('errors' => $errors['description'])); ?><?php endif; ?>
	<?php echo Form::textarea('answer', $params['answer'], array('placeholder' => 'こたえ', 'class' => 'span12')); ?>
	<?php if(isset($errors['answer'])): ?><?php echo View::forge('elements/errors', array('errors' => $errors['answer'])); ?><?php endif; ?>
	<button type="submit" class="btn btn-large btn-block btn-primary">ついかする</button>
<?php echo Form::close(); ?>