<?php !is_array($errors) and $errors = array($errors); ?>
<div class="alert alert-danger">
<?php foreach($errors as $error): ?>
	<p><?php echo $error instanceof Validation_Error ? $error->get_message() : $error; ?></p>
<?php endforeach; ?>
</div>