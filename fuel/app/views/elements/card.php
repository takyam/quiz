<h4>問題：<?php echo $question->title; ?></h4>
<?php if(!empty($question->description)): ?>
	<div class="well well-small"><?php echo nl2br($question->description); ?></div>
<?php endif; ?>
<div class="accordion" id="accordion">
	<div class="accordion-group">
		<div class="accordion-heading">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
				<i class="icon-star"></i> クリックして答えを見る
			</a>
		</div>
		<div id="collapseOne" class="accordion-body collapse">
			<div class="accordion-inner">
				<?php echo nl2br($question->answer); ?>
			</div>
		</div>
	</div>
</div>