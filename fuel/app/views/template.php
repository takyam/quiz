<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>Bootstrap, from Twitter</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<?php echo Asset::css(array('bootstrap.min.css')); ?>
	<?php echo Asset::css(array('bootstrap-responsive.min.css')); ?>
	<?php echo Asset::css(array('main.css')); ?>
</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="brand" href="/">ゆうちゃんくいず</a>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<li><a href="/">くいず</a></li>
					<li><a href="/top/create">ついか</a></li>
					<li><a href="/top/random">らんだむ</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<?php echo $content; ?>
		</div>
	</div>
</div>
<?php echo Asset::js(array('jquery.min.js', 'bootstrap.min.js')); ?>
</body>
</html>
