<div class="row-fluid">
	<div class="span4"></div>
	<div class="span4">
		<div class="well">
			<form class="form-signin" method="post">
				<h2 class="form-signin-heading">Please sign in</h2>
				<input type="password" name="password" placeholder="Password" class="span12">
				<button class="btn btn-block btn-large btn-primary" type="submit">Sign in</button>
			</form>
		</div>
	</div>
	<div class="span4"></div>
</div>