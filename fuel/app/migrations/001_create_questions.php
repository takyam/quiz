<?php

namespace Fuel\Migrations;

class Create_questions
{
	public function up()
	{
		\DBUtil::create_table(
			'questions',
			array(
				'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
				'title' => array('type' => 'text'),
				'description' => array('type' => 'text', 'null' => true),
				'answer' => array('type' => 'text'),
				'created_at' => array('type' => 'datetime'),
				'updated_at' => array('type' => 'datetime'),
			),
			array('id')
		);
	}

	public function down()
	{
		\DBUtil::drop_table('questions');
	}
}