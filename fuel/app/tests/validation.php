<?php
/**
 * @group App
 * @group AppValidation
 * @group Validation
 */
class Test_Validation extends TestCase
{
	public function test__validation_valid_single_line_text()
	{
		$val = Validation::forge(get_called_class().__FUNCTION__.'.1');
		$this->assertTrue($val->_validation_valid_single_line_text(''));
		$this->assertTrue($val->_validation_valid_single_line_text(null));
		$this->assertTrue($val->_validation_valid_single_line_text('A'));
		$this->assertTrue($val->_validation_valid_single_line_text('あああ'));
		$this->assertFalse($val->_validation_valid_single_line_text(PHP_EOL.'A'.PHP_EOL));
		$this->assertFalse($val->_validation_valid_single_line_text(PHP_EOL.'あああ'.PHP_EOL));
		$this->assertFalse($val->_validation_valid_single_line_text("\n"));
		$this->assertFalse($val->_validation_valid_single_line_text("\r"));
		$this->assertFalse($val->_validation_valid_single_line_text("\r\n"));
		$this->assertFalse($val->_validation_valid_single_line_text("\n\r"));
	}

	public function test__validation_valid_text_length()
	{
		$val = Validation::forge(get_called_class().__FUNCTION__.'.1');
		$this->assertTrue($val->_validation_valid_text_length(''));
		$this->assertTrue($val->_validation_valid_text_length(null));
		$this->assertTrue($val->_validation_valid_text_length('aaa'));
		$this->assertTrue($val->_validation_valid_text_length('あああ'));
		$this->assertTrue($val->_validation_valid_text_length('aaa'.PHP_EOL.'aaa'));
		$this->assertTrue($val->_validation_valid_text_length('あああ'.PHP_EOL.'あああ'));
		$this->assertTrue($val->_validation_valid_text_length(str_repeat('a', Validation::VALID_TEXT_LENGTH)));
		$this->assertTrue($val->_validation_valid_text_length(str_repeat('あ', Validation::VALID_TEXT_LENGTH)));
		$this->assertTrue($val->_validation_valid_text_length(str_repeat('a', Validation::VALID_TEXT_LENGTH - 1) . PHP_EOL));
		$this->assertTrue($val->_validation_valid_text_length(str_repeat('あ', Validation::VALID_TEXT_LENGTH - 1) . PHP_EOL));
		$this->assertFalse($val->_validation_valid_text_length(str_repeat('a', Validation::VALID_TEXT_LENGTH + 1)));
		$this->assertFalse($val->_validation_valid_text_length(str_repeat('あ', Validation::VALID_TEXT_LENGTH + 1)));
		$this->assertFalse($val->_validation_valid_text_length(str_repeat('a', Validation::VALID_TEXT_LENGTH) . PHP_EOL));
		$this->assertFalse($val->_validation_valid_text_length(str_repeat('あ', Validation::VALID_TEXT_LENGTH) . PHP_EOL));
	}
}