<?php
/**
 * @group App
 */
class Test_Model_Question extends TestCase
{
	public function data()
	{
		return array(
			array(
				array(
					'title' => 'たいとる',
					'description' => 'せつめい' . PHP_EOL . 'せつめい',
					'answer' => 'かいとう' . PHP_EOL . 'かいとう',
				),
			),
		);
	}

	/**
	 * @dataProvider data
	 */
	public function test_validate($success_params)
	{
		$this->assertInstanceOf(
			'Validation',
			$val = Model_Question::validate(get_called_class() . __FUNCTION__ . '.1')
		);
		$this->assertTrue($val->run($success_params));

		//タイトルは改行含むとNG
		$this->assertFalse($val->run(array('title' => 'title' . PHP_EOL . 'tiele') + $success_params));
		//タイトルは1万文字超えたらNG
		$this->assertFalse(
			$val->run(array('title' => str_repeat('aa', Validation::VALID_TEXT_LENGTH)) + $success_params)
		);
		//descriptionは1万文字超えたらNG
		$this->assertFalse(
			$val->run(array('description' => str_repeat('aa', Validation::VALID_TEXT_LENGTH)) + $success_params)
		);
		//answerは1万文字超えたらNG
		$this->assertFalse(
			$val->run(array('answer' => str_repeat('aa', Validation::VALID_TEXT_LENGTH)) + $success_params)
		);
	}

	/**
	 * @dataProvider data
	 */
	public function test_save_question($success_params)
	{
		$this->assertInstanceOf('Model_Question', $question = Model_Question::save_question($success_params, false));
		$this->assertEquals($success_params['title'], $question->title);
		$this->assertEquals($success_params['description'], $question->description);
		$this->assertEquals($success_params['answer'], $question->answer);

		//存在しないIDはあかん
		$this->setExpectedException('OutOfBoundsException');
		Model_Question::save_question(array('id' => 'Invalid Model ID') + $success_params, false);
	}
}