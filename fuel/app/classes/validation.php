<?php
class Validation extends Fuel\Core\Validation
{
	const VALID_TEXT_LENGTH = 10000;

	/**
	 * 文字列が改行を含まないかどうかを確認するバリデーション
	 * @param $value
	 * @return bool
	 */
	public function _validation_valid_single_line_text($value)
	{
		return $this->_empty($value) || !(preg_match('/[\n\r]/mu', $value) > 0);
	}

	/**
	 * 文字列長が1万文字を超えないかどうかを確認するバリデーション
	 * @param $value
	 * @return bool
	 */
	public function _validation_valid_text_length($value)
	{
		return $this->_empty($value) || mb_strlen($value, Config::get('encoding', 'UTF-8')) <= static::VALID_TEXT_LENGTH;
	}
}