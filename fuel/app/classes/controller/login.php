<?php
class Controller_Login extends Controller_Hybrid
{
	public function action_index()
	{
		$this->template->content = ViewModel::forge(
			'login/index',
			(strtolower(Input::method('get')) === 'post' ? 'post' : 'view')
		);
	}
}