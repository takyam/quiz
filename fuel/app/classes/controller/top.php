<?php
class Controller_Top extends Controller_Hybrid
{
	public function before()
	{
		parent::before();
		if (!Session::get('login', false)) {
			Response::redirect('login');
		}
	}

	public function action_index()
	{
		$this->template->content = ViewModel::forge('top/index');
	}

	public function action_create($question_id = null)
	{
		$this->template->content = ViewModel::forge(
			'top/create',
			(strtolower(Input::method('get')) === 'post' ? 'post' : 'view')
		);
		$this->template->content->set('question_id', $question_id);
	}

	public function action_random()
	{
		$this->template->content = ViewModel::forge('top/random');
	}

	public function action_show($question_id)
	{
		$this->template->content = ViewModel::forge('top/show');
		$this->template->content->set('question_id', $question_id);
	}

	public function action_delete($question_id)
	{
		$question = Model_Question::find($question_id);
		if(!$question){
			throw new HttpNotFoundException();
		}
		$question->delete();
		Response::redirect('top');
	}
}