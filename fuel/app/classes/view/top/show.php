<?php
class View_Top_Show extends ViewModel
{
	public function view()
	{
		if(!$question = Model_Question::find($this->get('question_id', null))){
			throw new HttpNotFoundException();
		}
		$this->set('question', $question);
	}
}