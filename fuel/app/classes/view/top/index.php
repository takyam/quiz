<?php
class View_Top_Index extends ViewModel
{
	public function view()
	{
		$questions = Model_Question::find('all');
		$this->set('questions', $questions);
	}
}