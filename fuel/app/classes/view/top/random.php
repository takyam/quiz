<?php
class View_Top_Random extends ViewModel
{
	public function view()
	{
		$last_question = Model_Question::find('last');
		if(!$last_question){
			throw new HttpNotFoundException();
		}
		$random_id = mt_rand(1, $last_question->id);
		$question = Model_Question::find('first', array('where' => array(array('id', '>=', $random_id))));
		if(!$question){
			$question = Model_Question::find('first', array('where' => array(array('id', '<=', $random_id))));
			if(!$question){
				throw new HttpNotFoundException();
			}
		}
		$this->set('question', $question);
	}
}