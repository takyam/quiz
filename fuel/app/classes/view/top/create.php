<?php
class View_Top_Create extends ViewModel
{
	public function view()
	{
		if($question_id = $this->get('question_id', null)){
			$question = Model_Question::find($question_id);
			if(!$question){
				throw new HttpNotFoundException();
			}
			$params = $question->to_array();
		}else{
			$params = array('title' => '', 'description' => '', 'answer' => '');
		}
		$this->set('params', $params);
	}

	public function post()
	{
		$params = array(
			'id' => $this->get('question_id', null),
			'title' => Input::post('title', null),
			'description' => Input::post('description', null),
			'answer' => Input::post('answer', null),
		);

		$validation = Model_Question::validate();
		if($validation->run($params)){
			try{
				$question = Model_Question::save_question($params);
				Response::redirect('top/show/' . $question->id);
			}catch(OutOfBoundsException $error){
				throw new HttpServerErrorException('不明なエラーにより保存に失敗しまいた');
			}
		}

		$this->set('errors', $validation->error());
		$this->set('params', $params);
	}
}