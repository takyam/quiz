<?php
class View_Login_Index extends ViewModel
{
	public function before()
	{
		if(Session::get('login', false)){
			Response::redirect('top');
		}
	}

	public function view(){}
	public function post()
	{
		Config::load('app', true);
		if($password = Config::get('app.password', null) and Input::post('password', null) === $password){
			Session::set('login', true);
			Response::redirect('top');
		}
	}
}