<?php

class Model_Question extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'title',
		'description',
		'answer',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
	protected static $_table_name = 'questions';

	protected static $_to_array_exclude = array('created_at', '');

	/**
	 * バリデータを返す
	 * @param string $factory
	 * @return Validation
	 */
	public static function validate($factory = 'save_new_question')
	{
		$val = Validation::forge($factory);
		$val->add_field('title', 'もんだい', 'required|valid_text_length|valid_single_line_text');
		$val->add_field('description', 'せつめい', 'valid_text_length');
		$val->add_field('answer', 'こたえ', 'required|valid_text_length');
		return $val;
	}

	/**
	 * 渡されたパラメータを元にQuestionを保存する
	 * @param array $params パラメータ
	 * @param bool $execute_save 保存を実行するかどうか
	 * @return static
	 * @throws OutOfBoundsException
	 */
	public static function save_question(array $params, $execute_save = true)
	{
		if (!isset($params['id']) || empty($params['id'])) {
			//IDが無ければ新規作成
			$question = static::forge();
		}else{
			//IDが有る場合はDBから取得
			$question = static::find($params['id']);
			if(!$question){
				throw new OutOfBoundsException('存在しないIDが指定されています');
			}
		}

		//パラメータをセット（or 更新）
		$question->title = $params['title'];
		$question->description = $params['description'];
		$question->answer = $params['answer'];

		//保存を行う
		if($execute_save){
			$question->save();
		}

		return $question;
	}
}
